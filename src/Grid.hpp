//
//  Grid.hpp
//  Wave_Maker
//
//  Created by Matthew Wheeler on 2/12/20.
//

#ifndef Grid_hpp
#define Grid_hpp

#include <stdio.h>
#include <ofMain.h>



class Gridclass{
public:
    
    void setup(int _y);
    void update(int _x, float _z);
    void draw();
    
    ofMesh grid;
    
    //index is the counter I use to set the index of each Vertex and UIndex is the index counter I use in the UPDATE section of the code.
    int index, UIndex, xOrigin,zOrigin;
    float height;
    ofColor teal;
    float noiseTime;


};


#endif /* Grid_hpp */

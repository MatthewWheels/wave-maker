#pragma once

#include "ofMain.h"
#include "Grid.hpp"
#include "ofxAssimpModelLoader.h"

#define nGrids 110

class ofApp : public ofBaseApp{
    
    //The General concept of this is to create a triangle strip mesh class which creates a single horizontal strip across. The class is then put into an array which generates several instances stacked on top of each other. The vertices of these meshes are to be affected by the brightness of a corresponding pixel from the webcam on the z axes. The idea is to make it look like a 3D wireframe of objects in the webcam.

    //Elements yet to be added: GUI control for the amplification of the z axis, GUI control to change the color of the tallest Vertexes
    
    public:
        void setup();
        void update();
        void draw();

    

 
    Gridclass grid[110];
    //camera
    ofEasyCam cam;
    //controls z value amplification
    float z;
    //These are each the object that they are named.
    ofBoxPrimitive road, railing, ground;
    ofxAssimpModelLoader mountain, building, building2, car, tree, tree2, tree3;
    string curFileInfo;
    
 

    
   
 
   
    

  
};

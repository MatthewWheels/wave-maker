//
//  Grid.cpp
//  Wave_Maker
//
//  Created by Matthew Wheeler on 2/12/20.
//

#include "Grid.hpp"

void Gridclass::setup(int _y){
    
    xOrigin=2500;
    zOrigin=-2000;
   //index is set to -1 so that the first vertex indexed is 0
       index=-1;
    
    
       glLineWidth(3);
    
    //This creates an array of points to create the triangle strip.
       for(int i=0; i<150; i++){
           
           grid.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
           
           //This set of commands creates 2 vertices starting with one below and then one above before moving along the x axis and repeating.
           //After each individual vertex is added the Index is increased and an index is added for that Vertex.
           
           grid.addVertex(glm::vec3(i*20+(xOrigin), 0, _y*20+zOrigin));
           grid.addColor(teal);
           index++;
           grid.addIndex(index);
         grid.addVertex(glm::vec3(i*20+(xOrigin), 0, (_y+1)*20+zOrigin));
           index++;
               grid.addColor(teal);
           grid.addIndex(index);
       }
    
}
void Gridclass::update(int _x, float _z){
    noiseTime+=0.01;
            //sets the U(pdate)Index to 0
          
             UIndex = 0;
            for(int j =0; j < 150; j++){
                float myNoise = 200.0*ofNoise(_x/40.0, j/40.0+(noiseTime/4));
                
              
            
          
                //sets the teal color
           
                                 teal.setHex(0x363ea7);
                 
               

                
                //Sets the vertex based upon the U(pdate)Index value and the brightness of the pixel color previously generated
                
                    grid.setVertex(UIndex, glm::vec3((xOrigin)+(j * 50), _z+(myNoise*3), _x*50+(myNoise*2)+zOrigin));
                
                
                //sets the color to teal

                  grid.setColor(UIndex,teal);


            
                
                UIndex++;
                
                //this gets the pixel color for the next vertex in the index
               myNoise = 200.0*ofNoise((_x+1)/40.0, j/40.0+(noiseTime/4));
                //sets the teal color
                
            
                                                       
           
                //sets the vertex and the color of the next vertex in the index
                 grid.setVertex((UIndex), glm::vec3((xOrigin)+(j * 50), _z+(myNoise*3), (_x+1)*50+(myNoise*2)+zOrigin));
                 grid.setColor(UIndex,teal);

                
                UIndex++;
                      
                

         
    
      
}
    }
void Gridclass::draw(){
    
    
 

    grid.draw();
    }




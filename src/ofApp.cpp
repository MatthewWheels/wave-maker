#include "ofApp.h"

//SEE APP.H FIRST

//--------------------------------------------------------------

void ofApp::setup(){

   
    //Sets cam at slightly left of center
  cam.setPosition(2500, 1000, 4200);
  
    
    //loads in all the models
   ofSetVerticalSync(true);
    mountain.loadModel("mountain.obj",20);
    curFileInfo = ".obj";
    building.loadModel("building.obj",20);
       curFileInfo = ".obj";
    building2.loadModel("building.obj",20);
          curFileInfo = ".obj";
    car.loadModel("car.obj",20);
    curFileInfo = ".obj";
    tree.loadModel("tree.obj",20);
    curFileInfo = ".obj";
    tree2.loadModel("tree.obj",20);
    curFileInfo = ".obj";
    tree3.loadModel("tree.obj",20);
    curFileInfo = ".obj";
   
    
   

    
    
//Grid SETUP
    for(int i=1; i<nGrids; i++){
             grid[i].setup(i);
         }
    z=100;
    //sets up the road, railing, and the ground objects
       road.set(900, 500, 6000);
       railing.set(45, 500, 6000);
    ground.set(4000,500,6000);
    }


//--------------------------------------------------------------
void ofApp::update(){

    
    
  
    for(int i=1; i<nGrids; i++){
             grid[i].update(i, z);
         }
}
  

//--------------------------------------------------------------
void ofApp::draw(){

   
    ofBackgroundHex(0x5fdbff);
    ofEnableDepthTest();
    cam.begin();
    

 

    
    
     for(int i=1; i<nGrids; i++){
      grid[i].draw();
      }
    
    //these create the trees lining the road
    ofSetHexColor(0x8c682f);
    tree.setScale(2,2,2);
    tree.setPosition(1500,700,3000);
    tree.setRotation(0,180,0,0,1);
    tree.drawFaces();
    
    
    tree2.setScale(2,2,2);
    tree2.setPosition(1500,700,1000);
    tree2.setRotation(0,180,0,0,1);
    tree2.drawFaces();
    
    tree3.setScale(2,2,2);
    tree3.setPosition(1500,700,2000);
    tree3.setRotation(0,180,0,0,1);
    tree3.drawFaces();
    
    //creates the car
    ofSetHexColor(0x018700);
    car.setRotation(0,180,0,0,1);
    car.setPosition(2550,700,3000);
    car.setScale(1.5,1.5,1.5);
    car.drawFaces();
    
    //creates the buildings
    ofSetHexColor(0xd6526e);
    building.setPosition(2000,3000,500);
    building.setScale(5,5,5);
    building.drawFaces();
    
    building2.setPosition(2000,3000,2500);
    building2.setScale(5,5,5);
    building2.drawFaces();
    
    //creates the mountains
    ofSetHexColor(0x3c3732);
       mountain.setPosition(3500,1000,-1800);
    mountain.setRotation(0, 135,0,1,0);
    mountain.setScale(20,20,20);
    mountain.drawFaces();

    //creates the ground
    ofSetHexColor(0x050052);
    ground.setPosition(-200,450,1000);
    ground.draw();
  
    //creates the road
    ofSetHexColor(0xaeaac8);
road.setPosition(2300,450,1000);
    road.draw();
    
    //creates the railing
    ofSetHexColor(0x8e7e8e);
    railing.setPosition(2800,600,1000);
    railing.draw();

   
 
    
    
  
   
    cam.end();
   
    cam.draw();

 
 
   
    ofDisableDepthTest();
 
 
    
}

//--------------------------------------------------------------



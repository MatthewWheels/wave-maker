# Wave Maker
By Matthew Wheeler

This program creates a color coded 3D environment which can be recorded and the run through the COCO model on RunwayML to achieve a weridly real coastline.

A Screenshot of the scene before being put through COCO. 

![](https://bitbucket.org/MatthewWheels/wave-maker/raw/dd1c64410fb8c84e6c49e107361f9788627cf7e0/Screenshots/Pre_COCO.png)


A Screenshot of the scene after being put through COCO. 

![](https://bitbucket.org/MatthewWheels/wave-maker/raw/dd1c64410fb8c84e6c49e107361f9788627cf7e0/Screenshots/Post_COCO.png)

